<?php
/**
 * Created by PhpStorm.
 * User: c5101
 * Date: 2017/5/21
 * Time: 下午 01:29
 */

namespace Tpk\LaravelUpload;

use Tpk\Means\Tool;

class Upload extends UploadFunction
{
    private $type;
    private $name;
    private $size;

    /**
     * 取得上傳檔案
     *
     * @param $file
     *
     * @return $this
     */
    public function setFile ($file)
    {
        $this->file = $file;
        $this->type = $file->extension();
        $this->info = strtolower($file->getClientOriginalExtension());
        $this->name = $file->getClientOriginalName();
        $this->size = $file->getClientSize();

        self::Disk($this->type);

        return $this;
    }

    /**
     * 顯示執行結果
     */
    public function show ()
    {
        if(!isset($this->file)){
            self::addMessage('error','沒有檔案被上傳（沒有選擇上傳檔案就送出表單）');
        }
        foreach (self::$message as $index => $msg) {
            flash(implode('<br>', $msg))->$index();
        }
    }

    public function info ()
    {
        Tool::Output($this);
        Tool::Output(pathinfo(storage_path().'/images/'.$this->name));
        Tool::Output(getimagesize(\Storage::url('/images/'.$this->name)));

        return $this;
    }

    /**
     * 將上傳之檔案進行儲存
     * @return $this
     */
    public function save ()
    {
        $checkFile = $this->AllowFile();
        $checkSize = $this->AllowSize();

        if ($checkFile && $checkSize) {
            $this->file->store(self::Disk($this->type));
            self::addMessage('success', "檔案已成功上傳(" . $this->name . ")");
        }

        return $this;
    }

    /**
     * 判斷上傳之檔案是否放行
     * 允許之類型放置在 WhiteList()
     * @return bool
     */
    private function AllowFile ()
    {
        $rules = $this->WhiteList($this->type);

        if ($this->info === 'jpg' && $this->type === 'jpeg') {
            return true;
        }

        if ($this->info !== $this->type) {
            $error_message = '格式有誤，無法上傳該檔案。(' . $this->name . ')';
            self::addMessage('error', $error_message);

            return false;
        }

        if (empty($rules)) {
            $error_message = "{$this->type}  類型不允許上傳";
            self::addMessage('error', $error_message);

            return false;
        }

        return true;
    }

    /**
     * 可允許檔案大小
     * 以Byte處理
     * Image => 2MB, File => 5MB
     */
    private function AllowSize ()
    {
        if ($this->size > Tool::Conversion(self::Size())) {
            self::addMessage('error', '該檔案超過允許檔案大小(' . $this->name . ')');

            return false;
        }

        return true;
    }
}