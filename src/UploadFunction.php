<?php
/**
 * Created by PhpStorm.
 * User: c5101
 * Date: 2017/5/24
 * Time: 下午 11:22
 */

namespace Tpk\LaravelUpload;

class UploadFunction
{
    public static $errors = [];
    public static $message = [];
    protected static $disk;

    /**
     * 設定存放位置
     *
     * @param $disk
     *
     * @return $this
     */
    protected static function Disk ($type)
    {
        self::$disk = self::WhiteList($type);

        return self::$disk;
    }

    /**
     * 上傳白名單
     *
     * @param $type
     *
     * @return mixed
     */
    protected static function WhiteList ($type)
    {
        $array = [
            'images' => ['gif', 'png', 'jpg', 'jpeg'],
            'files'  => ['pdf', 'doc', 'docx', 'xls', 'xlsx'],
        ];

        foreach ($array as $k => $t) {
            if (in_array($type, $t))
                return $k;
        }

        return false;
    }

    protected static function Size ()
    {
        $images = '2MB';
        $files = '5MB';

        return ${self::$disk};
    }

    protected static function addMessage ($type, $message)
    {
        self::$message["{$type}"][] = $message;
    }
}